// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SnakeActor.generated.h"

class ASnakeElementActor;

UENUM()
enum class EMovementDirection
{
	UP,
	DOWN,
	LEFT,
	RIGHT
};

UCLASS()
class SNAKE_API ASnakeActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASnakeActor();

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<ASnakeElementActor> SnakeElementActorClass;

	UPROPERTY(EditDefaultsOnly)
		float ElementSize = 100.0;

	UPROPERTY(EditDefaultsOnly)
		float MovementSpeed = 10.0;

	UPROPERTY()
		TArray<ASnakeElementActor*> SnakeElements;

	UPROPERTY()
		EMovementDirection LastMovementDirection = EMovementDirection::UP;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
		void SnakeElementOverlap(ASnakeElementActor* SnakeElementActor, AActor* OtherActor);

	void AddSnakeElement(int32_t ElementsNum = 1);

	void Move();

	void SnakeDestroy();

	[[nodiscard]] const float GetElementSize()
	{
		return ElementSize;
	}
};
