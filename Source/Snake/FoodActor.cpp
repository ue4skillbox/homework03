// Fill out your copyright notice in the Description page of Project Settings.

#include "FoodActor.h"
#include "SnakeActor.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"

// Sets default values
AFoodActor::AFoodActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	StaticMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	StaticMeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	StaticMeshComponent->SetCollisionResponseToAllChannels(ECR_Overlap);
	SetActorLocation(GenerateNewLocation(FVector()));
}

// Called when the game starts or when spawned
void AFoodActor::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AFoodActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AFoodActor::Interact(AActor* Interactor, bool IsHead, const FVector& snake_location)
{
	if (IsHead)
	{
		ASnakeActor* SnakeActor = Cast<ASnakeActor>(Interactor);
		if (IsValid(SnakeActor))
		{
			SnakeActor->AddSnakeElement();
			this->SetActorLocation(GenerateNewLocation(snake_location));
		}
	}
}

FVector AFoodActor::GenerateNewLocation(const FVector& snake_location)
{
	FVector result = FVector();
	do
	{
		float x_rand = x_range - std::rand() % ((int)x_range * 2 + 1);
		float y_rand = y_range - std::rand() % ((int)y_range * 2 + 1);
		result = FVector(x_rand, y_rand, 0);
	}
	while (result.X == snake_location.X && result.Y == snake_location.Y);
	return result;
}
