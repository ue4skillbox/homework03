// Fill out your copyright notice in the Description page of Project Settings.

#include "SnakeActor.h"
#include "Interactable.h"
#include "SnakeElementActor.h"
#

// Sets default values
ASnakeActor::ASnakeActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ASnakeActor::BeginPlay()
{
	Super::BeginPlay();
	SetActorTickInterval(MovementSpeed);
	AddSnakeElement(1);
}

// Called every frame
void ASnakeActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	Move();
}

void ASnakeActor::AddSnakeElement(int32_t ElementsNum)
{
	for (int32_t i = 0; i < ElementsNum; ++i)
	{
		FVector new_element_location(ForceInitToZero);
		if (SnakeElements.Num() > 0)
		{
			auto last_element = SnakeElements.Last();
			FVector last_element_location = last_element->GetActorLocation();
			for (auto& e : SnakeElements)
			{
				auto element = e->GetActorLocation();
			}
			switch (LastMovementDirection)
			{
			case EMovementDirection::UP:
				new_element_location = last_element_location - FVector(ElementSize, 0, 0);
				break;
			case EMovementDirection::DOWN:
				new_element_location = last_element_location + FVector(ElementSize, 0, 0);
				break;
			case EMovementDirection::LEFT:
				new_element_location = last_element_location - FVector(0, ElementSize, 0);
				break;
			case EMovementDirection::RIGHT:
				new_element_location = last_element_location + FVector(0, ElementSize, 0);
				break;
			default:
				break;
			}
		}
		else
		{
			new_element_location = GetActorLocation() - FVector(ElementSize, 0, 0);
		}
		FActorSpawnParameters params;
		params.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
		ASnakeElementActor* new_element = GetWorld()->SpawnActor<ASnakeElementActor>(SnakeElementActorClass, FTransform(new_element_location), params);
		new_element->ElementOwner = this;
		size_t index = SnakeElements.Add(new_element);
		if (index == 0)
		{
			new_element->SetFirstElementType();
		}
	}
}

void ASnakeActor::Move()
{
	FVector Movement(ForceInitToZero);

	switch (LastMovementDirection)
	{
	case EMovementDirection::UP:
		Movement.X += ElementSize;
		break;
	case EMovementDirection::DOWN:
		Movement.X -= ElementSize;
		break;
	case EMovementDirection::LEFT:
		Movement.Y += ElementSize;
		break;
	case EMovementDirection::RIGHT:
		Movement.Y -= ElementSize;
		break;
	default:
		break;
	}

	FVector prev_location = SnakeElements[0]->GetActorLocation();
	SnakeElements[0]->AddActorWorldOffset(Movement);

	for (size_t i = 1; i < SnakeElements.Num(); ++i)
	{
		FVector curr_location = SnakeElements[i]->GetActorLocation();
		SnakeElements[i]->SetActorLocation(prev_location);
		prev_location = curr_location;
	}
}

void ASnakeActor::SnakeDestroy()
{
	for (auto& element : SnakeElements)
	{
		element->Destroy();
	}
	this->Destroy();
}

void ASnakeActor::SnakeElementOverlap(ASnakeElementActor* SnakeElementActor, AActor* OtherActor)
{
	if (IsValid(SnakeElementActor))
	{
		int32_t index; 
		SnakeElements.Find(SnakeElementActor, index);
		bool is_first = index == 0;
		IInteractable* InteractableInterface = Cast<IInteractable>(OtherActor);
		if (InteractableInterface != nullptr)
		{
			InteractableInterface->Interact(this, is_first, this->GetActorLocation());
		}
		else
		{
			SnakeDestroy();
		}
	}
}