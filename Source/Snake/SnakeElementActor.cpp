// Fill out your copyright notice in the Description page of Project Settings.

#include "SnakeElementActor.h"
#include "SnakeActor.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"

// Sets default values
ASnakeElementActor::ASnakeElementActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	StaticMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	StaticMeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	StaticMeshComponent->SetCollisionResponseToAllChannels(ECR_Overlap);
}

// Called when the game starts or when spawned
void ASnakeElementActor::BeginPlay()
{
	Super::BeginPlay();

}

// Called every frame
void ASnakeElementActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ASnakeElementActor::HandleBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, 
	UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (IsValid(ElementOwner))
	{
		ElementOwner->SnakeElementOverlap(this, OtherActor);
	}
}

void ASnakeElementActor::ToggleCollision()
{
	if (StaticMeshComponent->GetCollisionEnabled() == ECollisionEnabled::NoCollision)
	{
		StaticMeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	}
	else
	{
		StaticMeshComponent->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	}
}

void ASnakeElementActor::Interact(AActor* Interactor, bool IsHead, const FVector& snake_location)
{
	ASnakeActor* Snake = Cast<ASnakeActor>(Interactor);
	if (IsValid(Snake))
	{
		Snake->SnakeDestroy();
	}
}

