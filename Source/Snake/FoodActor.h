// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Interactable.h"
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "FoodActor.generated.h"

class UStaticMeshComponent;

UCLASS()
class SNAKE_API AFoodActor : public AActor, public IInteractable
{
	GENERATED_BODY()

public:	
	// Sets default values for this actor's properties
	AFoodActor();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		UStaticMeshComponent* StaticMeshComponent;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void Interact(AActor* Interactor, bool IsHead, const FVector& snake_location) override;

	void SetRange(float x, float y)
	{
		x_range = x;
		y_range = y;
	}

	[[nodiscard]] FVector GenerateNewLocation(const FVector& snake_location);

private:
	float x_range = 500.0;
	float y_range = 750.0;

};
