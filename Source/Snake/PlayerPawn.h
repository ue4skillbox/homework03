// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "PlayerPawn.generated.h"

class UCameraComponent;
class ASnakeActor;
class AFoodActor;

UCLASS()
class SNAKE_API APlayerPawn : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	APlayerPawn();

	UPROPERTY(BlueprintReadWrite)
		UCameraComponent* CameraComponent;

	UPROPERTY(BlueprintReadWrite)
		ASnakeActor* SnakeActor;

	UPROPERTY(BlueprintReadWrite)
		AFoodActor* FoodActor;

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<ASnakeActor> SnakeActorClass;

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<AFoodActor> FoodActorClass;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float XRange = 1.0;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float YRange = 1.0;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UFUNCTION()
		void HandlePlayerVerticalInput(float value);

	UFUNCTION()
		void HandlePlayerHorizontalInput(float value);

private:
	void CreateSnakeActor();
	void CreateFoodActor();

};
